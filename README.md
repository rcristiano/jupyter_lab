# Container with JupyterLab

Base image: [jupyter/all-spark-notebook](https://hub.docker.com/r/jupyter/all-spark-notebook)

## Create container

```sh
docker-compose up -d --build
```

## Accessing 

```sh
# Open in your browser
xdg-open 'http://localhost:'$(docker-compose logs | grep -oG :\[0-9\].\* | tail -1)
xdg-open $(docker logs jupyter_lab 2>&1 | grep -oP http:\/\/1.\*token.\* | tail -1)
# or
docker-compose logs jupyter_lab | grep -oP http:\/\/1.\*token.\* | tail -1
# or
docker logs jupyter_lab 2>&1 | grep -oP http:\/\/1.\*token.\* | tail -1
# and open in your browser
```

## Add python kernel from virtual envs

* _On JupyterLab terminal_

    ```sh
    conda create -n py27 python=2.7 ipykernel
    source activate py27
    ipython kernel install --user --name=python2.7 --display-name "Python 2.7"
    ```

### Obs

> Special package `nb_conda_kernels` that detects conda environments with notebook kernels and automatically registers them. This makes using a new python version as easy as creating new conda environments:
>
><https://github.com/Anaconda-Platform/nb_conda_kernels>

```sh
conda create -n py27 python=2.7 ipykernel
conda create -n py36 python=3.6 ipykernel
```

> Run `jupyter kernelspec list` to get the paths of all your kernels.
Then simply uninstall your unwanted-kernel

```sh
jupyter kernelspec uninstall unwanted-kernel
```

## Todo

    [ ] Persist Fine Customizations
    [ ] Automation script
