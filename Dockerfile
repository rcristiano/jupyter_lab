FROM jupyter/all-spark-notebook

RUN conda update --quiet --yes conda && \
    conda install --quiet --yes \
    'nb_conda_kernels' && \
    conda create --quiet --yes \
    --name Python_2.7 python=2.7 \
    ipykernel pandas
